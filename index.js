exports.fromExpress = fn => async (req, res, next) => {
  await new Promise((resolve, reject) => {
    try {
      fn(req, res, resolve);
    } catch (err) {
      reject(err);
    }
  });
  return next();
};
